#!/bin/bash
if [ "$#" -ne "1" ]; then
  echo "************************************"
  echo "$0 action preseedbuckets"
  echo "usage: $0 create"
  echo "usage: $0 update"
  echo "usage: $0 delete"
  echo "************************************"
  exit 1;
fi

aws_user_name=$(aws iam get-user --output text | awk '{print $NF}')
stackname=$aws_user_name-gitlab-ha


if [ "$1" == "create" ]; then

  echo
  echo " creating $stackname against s3://proservcfbucket/$aws_user_name/"
  echo
  aws s3 cp . s3://proservcfbucket/$aws_user_name/ --recursive
  sleep 1
  aws cloudformation create-stack                                                                             \
    --capabilities CAPABILITY_NAMED_IAM CAPABILITY_AUTO_EXPAND CAPABILITY_IAM                                 \
    --template-url https://proservcfbucket.s3.amazonaws.com/$aws_user_name/gitlab_ha.cfn.yaml                \
    --stack-name $stackname                                                                                   \
    --parameters file:////$(pwd)/json_parameters/gitlab_ha_parameters.json

fi

if [ "$1" == "update" ]; then
  echo
  echo " updating $stackname against s3://proservcfbucket/$aws_user_name/"
  echo
  aws s3 cp . s3://proservcfbucket/$aws_user_name/ --recursive

  aws cloudformation update-stack                                                                             \
    --capabilities CAPABILITY_NAMED_IAM CAPABILITY_AUTO_EXPAND CAPABILITY_IAM                                 \
    --template-url https://proservcfbucket.s3.amazonaws.com/$aws_user_name/gitlab_ha.cfn.yaml               \
    --stack-name $stackname                                                                                   \
    --parameters file:///$(pwd)/json_parameters/gitlab_ha_parameters.json

fi

if [ "$1" == "delete" ]; then
  echo
  echo " Deleting $stackname"
  echo " This will require manual intervention for S3"
  echo "TODO Clean up S3 Buckets before Hand"
  echo
  aws cloudformation delete-stack --stack-name $stackname
fi
