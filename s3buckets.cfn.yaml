---
AWSTemplateFormatVersion: '2010-09-09'
Description: >
  GitLab Top Level CloudFormation Template.
  Load Buckets with aws s3 sync s3://gitlabexampleconfig s3://kmv-config-gitlab --sse-kms-key-id <bucketkeyid> --sse aws:kms


Parameters:
  CMKArn:
    Description: Arn of the S3 Encryption Key
    Type: String

  BucketPrefix:
    Description: Prefix For Bucket Names. Ending Dash is appended.
    Type: String
    Default: kmv
    AllowedPattern: ^[a-z]*
    ConstraintDescription: Only Allows Lower Case Letters

  BucketSuffix:
    Description: Suffix For Bucket Names.
    Type: String
    Default: gitlab
    AllowedPattern: ^[a-z]*
    ConstraintDescription: Only Allows Lower Case Letters

Resources:

#### S3 Buckets Begin #####
  ConfigBucket:
    Type: AWS::S3::Bucket
    Properties:
      BucketName: !Sub ${BucketPrefix}-config-${BucketSuffix}
      BucketEncryption:
        ServerSideEncryptionConfiguration:
        - ServerSideEncryptionByDefault:
            SSEAlgorithm: aws:kms
            KMSMasterKeyID: !Ref CMKArn
          BucketKeyEnabled: true
      PublicAccessBlockConfiguration:
        BlockPublicAcls: true
        BlockPublicPolicy: true
        IgnorePublicAcls: true
        RestrictPublicBuckets: true

  ConfigBucketBucketPolicy:
    Type: AWS::S3::BucketPolicy
    Properties:
      Bucket:
        !Ref ConfigBucket
      PolicyDocument:
        Statement:

          - Sid: DenyIncorrectCMK
            Action: s3:PutObject
            Effect: Deny                      
            Resource: !Sub ${ConfigBucket.Arn}/*
            Principal: '*'
            Condition:
              StringNotEquals: 
                s3:x-amz-server-side-encryption-aws-kms-key-id: !Ref CMKArn

          - Sid: DenyIncorrectEncryptionHeader
            Action: s3:PutObject
            Effect: Deny
            Resource: !Sub ${ConfigBucket.Arn}/*
            Principal: '*'
            Condition:
              StringNotEquals:
                s3:x-amz-server-side-encryption: 'aws:kms'

          - Sid: DenyUnEncryptedObjectUploads
            Action: s3:PutObject
            Effect: Deny
            Resource: !Sub ${ConfigBucket.Arn}/*
            Principal: '*'
            Condition:
              StringNotEquals:
                s3:x-amz-server-side-encryption-aws-kms-key-id: !Ref CMKArn

          - Sid: denyHTTP
            Action: s3:*
            Effect: Deny
            Resource: !Sub ${ConfigBucket.Arn}/*
            Principal: '*'
            Condition:
              Bool:
                aws:SecureTransport: 'false'  

  ArtifactBucket:
    Type: AWS::S3::Bucket
    Properties:
      BucketName: !Sub ${BucketPrefix}-artifact-${BucketSuffix}
      BucketEncryption:
        ServerSideEncryptionConfiguration:
        - ServerSideEncryptionByDefault:
            SSEAlgorithm: aws:kms
            KMSMasterKeyID: !Ref CMKArn
          BucketKeyEnabled: true
      PublicAccessBlockConfiguration:
        BlockPublicAcls: true
        BlockPublicPolicy: true
        IgnorePublicAcls: true
        RestrictPublicBuckets: true
      LifecycleConfiguration:
        Rules:
        - Id: IATransition
          Status: Enabled
          Transitions:
          - StorageClass: STANDARD_IA
            TransitionInDays: 30

  ArtifactBucketPolicy:
    Type: AWS::S3::BucketPolicy
    Properties:
      Bucket:
        !Ref ArtifactBucket
      PolicyDocument:
        Statement:

          - Sid: DenyIncorrectCMK
            Action: s3:PutObject
            Effect: Deny                      
            Resource: !Sub ${ArtifactBucket.Arn}/*
            Principal: '*'
            Condition:
              StringNotEquals: 
                s3:x-amz-server-side-encryption-aws-kms-key-id: !Ref CMKArn

          - Sid: DenyIncorrectEncryptionHeader
            Action: s3:PutObject
            Effect: Deny
            Resource: !Sub ${ArtifactBucket.Arn}/*
            Principal: '*'
            Condition:
              StringNotEquals:
                s3:x-amz-server-side-encryption: 'aws:kms'

          - Sid: DenyUnEncryptedObjectUploads
            Action: s3:PutObject
            Effect: Deny
            Resource: !Sub ${ArtifactBucket.Arn}/*
            Principal: '*'
            Condition:
              StringNotEquals:
                s3:x-amz-server-side-encryption-aws-kms-key-id: !Ref CMKArn

          - Sid: denyHTTP
            Action: s3:*
            Effect: Deny
            Resource: !Sub ${ArtifactBucket.Arn}/*
            Principal: '*'
            Condition:
              Bool:
                aws:SecureTransport: 'false'  

  ExtDiffsBucket:
    Type: AWS::S3::Bucket
    Properties:
      BucketName: !Sub ${BucketPrefix}-extdiffs-${BucketSuffix}
      BucketEncryption:
        ServerSideEncryptionConfiguration:
        - ServerSideEncryptionByDefault:
            SSEAlgorithm: aws:kms
            KMSMasterKeyID: !Ref CMKArn
          BucketKeyEnabled: true            
      PublicAccessBlockConfiguration:
        BlockPublicAcls: true
        BlockPublicPolicy: true
        IgnorePublicAcls: true
        RestrictPublicBuckets: true
      LifecycleConfiguration:
        Rules:
        - Id: IATransition
          Status: Enabled
          Transitions:
          - StorageClass: STANDARD_IA
            TransitionInDays: 30

  ExtDiffsBucketPolicy:
    Type: AWS::S3::BucketPolicy
    Properties:
      Bucket:
        !Ref ExtDiffsBucket
      PolicyDocument:
        Statement:

          - Sid: DenyIncorrectCMK
            Action: s3:PutObject
            Effect: Deny                      
            Resource: !Sub ${ExtDiffsBucket.Arn}/*
            Principal: '*'
            Condition:
              StringNotEquals: 
                s3:x-amz-server-side-encryption-aws-kms-key-id: !Ref CMKArn

          - Sid: DenyIncorrectEncryptionHeader
            Action: s3:PutObject
            Effect: Deny
            Resource: !Sub ${ExtDiffsBucket.Arn}/*
            Principal: '*'
            Condition:
              StringNotEquals:
                s3:x-amz-server-side-encryption: 'aws:kms'

          - Sid: DenyUnEncryptedObjectUploads
            Action: s3:PutObject
            Effect: Deny
            Resource: !Sub ${ExtDiffsBucket.Arn}/*
            Principal: '*'
            Condition:
              StringNotEquals:
                s3:x-amz-server-side-encryption-aws-kms-key-id: !Ref CMKArn

          - Sid: denyHTTP
            Action: s3:*
            Effect: Deny
            Resource: !Sub ${ExtDiffsBucket.Arn}/*
            Principal: '*'
            Condition:
              Bool:
                aws:SecureTransport: 'false'  

  lfsBucket:
    Type: AWS::S3::Bucket
    Properties:  
      BucketName: !Sub ${BucketPrefix}-lfs-${BucketSuffix}
      BucketEncryption:
        ServerSideEncryptionConfiguration:
        - ServerSideEncryptionByDefault:
            SSEAlgorithm: aws:kms
            KMSMasterKeyID: !Ref CMKArn
          BucketKeyEnabled: true            
      PublicAccessBlockConfiguration:
        BlockPublicAcls: true
        BlockPublicPolicy: true
        IgnorePublicAcls: true
        RestrictPublicBuckets: true
      LifecycleConfiguration:
        Rules:
        - Id: IATransition
          Status: Enabled
          Transitions:
          - StorageClass: STANDARD_IA
            TransitionInDays: 30

  lfsBucketPolicy:
    Type: AWS::S3::BucketPolicy
    Properties:
      Bucket:
        !Ref lfsBucket
      PolicyDocument:
        Statement:

          - Sid: DenyIncorrectCMK
            Action: s3:PutObject
            Effect: Deny                      
            Resource: !Sub ${lfsBucket.Arn}/*
            Principal: '*'
            Condition:
              StringNotEquals: 
                s3:x-amz-server-side-encryption-aws-kms-key-id: !Ref CMKArn

          - Sid: DenyIncorrectEncryptionHeader
            Action: s3:PutObject
            Effect: Deny
            Resource: !Sub ${lfsBucket.Arn}/*
            Principal: '*'
            Condition:
              StringNotEquals:
                s3:x-amz-server-side-encryption: 'aws:kms'

          - Sid: DenyUnEncryptedObjectUploads
            Action: s3:PutObject
            Effect: Deny
            Resource: !Sub ${lfsBucket.Arn}/*
            Principal: '*'
            Condition:
              StringNotEquals:
                s3:x-amz-server-side-encryption-aws-kms-key-id: !Ref CMKArn

          - Sid: denyHTTP
            Action: s3:*
            Effect: Deny
            Resource: !Sub ${lfsBucket.Arn}/*
            Principal: '*'
            Condition:
              Bool:
                aws:SecureTransport: 'false'  

  PackagesBucket:
    Type: AWS::S3::Bucket
    Properties:  
      BucketName: !Sub ${BucketPrefix}-packages-${BucketSuffix}
      BucketEncryption:
        ServerSideEncryptionConfiguration:
        - ServerSideEncryptionByDefault:
            SSEAlgorithm: aws:kms
            KMSMasterKeyID: !Ref CMKArn
          BucketKeyEnabled: true            
      PublicAccessBlockConfiguration:
        BlockPublicAcls: true
        BlockPublicPolicy: true
        IgnorePublicAcls: true
        RestrictPublicBuckets: true
      LifecycleConfiguration:
        Rules:
        - Id: IATransition
          Status: Enabled
          Transitions:
          - StorageClass: STANDARD_IA
            TransitionInDays: 30

  PackagesBucketPolicy:
    Type: AWS::S3::BucketPolicy
    Properties:
      Bucket:
        !Ref PackagesBucket
      PolicyDocument:
        Statement:

          - Sid: DenyIncorrectCMK
            Action: s3:PutObject
            Effect: Deny                      
            Resource: !Sub ${PackagesBucket.Arn}/*
            Principal: '*'
            Condition:
              StringNotEquals: 
                s3:x-amz-server-side-encryption-aws-kms-key-id: !Ref CMKArn

          - Sid: DenyIncorrectEncryptionHeader
            Action: s3:PutObject
            Effect: Deny
            Resource: !Sub ${PackagesBucket.Arn}/*
            Principal: '*'
            Condition:
              StringNotEquals:
                s3:x-amz-server-side-encryption: 'aws:kms'

          - Sid: DenyUnEncryptedObjectUploads
            Action: s3:PutObject
            Effect: Deny
            Resource: !Sub ${PackagesBucket.Arn}/*
            Principal: '*'
            Condition:
              StringNotEquals:
                s3:x-amz-server-side-encryption-aws-kms-key-id: !Ref CMKArn

          - Sid: denyHTTP
            Action: s3:*
            Effect: Deny
            Resource: !Sub ${PackagesBucket.Arn}/*
            Principal: '*'
            Condition:
              Bool:
                aws:SecureTransport: 'false' 

  DepProxyBucket:
    Type: AWS::S3::Bucket
    Properties:
      BucketName: !Sub ${BucketPrefix}-depproxy-${BucketSuffix}
      BucketEncryption:
        ServerSideEncryptionConfiguration:
        - ServerSideEncryptionByDefault:
            SSEAlgorithm: aws:kms
            KMSMasterKeyID: !Ref CMKArn
          BucketKeyEnabled: true            
      PublicAccessBlockConfiguration:
        BlockPublicAcls: true
        BlockPublicPolicy: true
        IgnorePublicAcls: true
        RestrictPublicBuckets: true
      LifecycleConfiguration:
        Rules:
        - Id: IATransition
          Status: Enabled
          Transitions:
          - StorageClass: STANDARD_IA
            TransitionInDays: 30

  DepProxyBucketPolicy:
    Type: AWS::S3::BucketPolicy
    Properties:
      Bucket:
        !Ref DepProxyBucket
      PolicyDocument:
        Statement:

          - Sid: DenyIncorrectCMK
            Action: s3:PutObject
            Effect: Deny                      
            Resource: !Sub ${DepProxyBucket.Arn}/*
            Principal: '*'
            Condition:
              StringNotEquals: 
                s3:x-amz-server-side-encryption-aws-kms-key-id: !Ref CMKArn

          - Sid: DenyIncorrectEncryptionHeader
            Action: s3:PutObject
            Effect: Deny
            Resource: !Sub ${DepProxyBucket.Arn}/*
            Principal: '*'
            Condition:
              StringNotEquals:
                s3:x-amz-server-side-encryption: 'aws:kms'

          - Sid: DenyUnEncryptedObjectUploads
            Action: s3:PutObject
            Effect: Deny
            Resource: !Sub ${DepProxyBucket.Arn}/*
            Principal: '*'
            Condition:
              StringNotEquals:
                s3:x-amz-server-side-encryption-aws-kms-key-id: !Ref CMKArn

          - Sid: denyHTTP
            Action: s3:*
            Effect: Deny
            Resource: !Sub ${DepProxyBucket.Arn}/*
            Principal: '*'
            Condition:
              Bool:
                aws:SecureTransport: 'false' 

  TfStateBucket:
    Type: AWS::S3::Bucket
    Properties:
      BucketName: !Sub ${BucketPrefix}-tfstate-${BucketSuffix}
      BucketEncryption:
        ServerSideEncryptionConfiguration:
        - ServerSideEncryptionByDefault:
            SSEAlgorithm: aws:kms
            KMSMasterKeyID: !Ref CMKArn
          BucketKeyEnabled: true            
      PublicAccessBlockConfiguration:
        BlockPublicAcls: true
        BlockPublicPolicy: true
        IgnorePublicAcls: true
        RestrictPublicBuckets: true
      LifecycleConfiguration:
        Rules:
        - Id: IATransition
          Status: Enabled
          Transitions:
          - StorageClass: STANDARD_IA
            TransitionInDays: 30

  TfStateBucketPolicy:
    Type: AWS::S3::BucketPolicy
    Properties:
      Bucket:
        !Ref TfStateBucket
      PolicyDocument:
        Statement:

          - Sid: DenyIncorrectCMK
            Action: s3:PutObject
            Effect: Deny                      
            Resource: !Sub ${TfStateBucket.Arn}/*
            Principal: '*'
            Condition:
              StringNotEquals: 
                s3:x-amz-server-side-encryption-aws-kms-key-id: !Ref CMKArn

          - Sid: DenyIncorrectEncryptionHeader
            Action: s3:PutObject
            Effect: Deny
            Resource: !Sub ${TfStateBucket.Arn}/*
            Principal: '*'
            Condition:
              StringNotEquals:
                s3:x-amz-server-side-encryption: 'aws:kms'

          - Sid: DenyUnEncryptedObjectUploads
            Action: s3:PutObject
            Effect: Deny
            Resource: !Sub ${TfStateBucket.Arn}/*
            Principal: '*'
            Condition:
              StringNotEquals:
                s3:x-amz-server-side-encryption-aws-kms-key-id: !Ref CMKArn

          - Sid: denyHTTP
            Action: s3:*
            Effect: Deny
            Resource: !Sub ${TfStateBucket.Arn}/*
            Principal: '*'
            Condition:
              Bool:
                aws:SecureTransport: 'false' 

  PagesBucket:
    Type: AWS::S3::Bucket
    Properties:
      BucketName: !Sub ${BucketPrefix}-pages-${BucketSuffix}
      BucketEncryption:
        ServerSideEncryptionConfiguration:
        - ServerSideEncryptionByDefault:
            SSEAlgorithm: aws:kms
            KMSMasterKeyID: !Ref CMKArn
          BucketKeyEnabled: true            
      PublicAccessBlockConfiguration:
        BlockPublicAcls: true
        BlockPublicPolicy: true
        IgnorePublicAcls: true
        RestrictPublicBuckets: true
      LifecycleConfiguration:
        Rules:
        - Id: IATransition
          Status: Enabled
          Transitions:
          - StorageClass: STANDARD_IA
            TransitionInDays: 30

  PagesBucketPolicy:
    Type: AWS::S3::BucketPolicy
    Properties:
      Bucket:
        !Ref PagesBucket
      PolicyDocument:
        Statement:

          - Sid: DenyIncorrectCMK
            Action: s3:PutObject
            Effect: Deny                      
            Resource: !Sub ${PagesBucket.Arn}/*
            Principal: '*'
            Condition:
              StringNotEquals: 
                s3:x-amz-server-side-encryption-aws-kms-key-id: !Ref CMKArn

          - Sid: DenyIncorrectEncryptionHeader
            Action: s3:PutObject
            Effect: Deny
            Resource: !Sub ${PagesBucket.Arn}/*
            Principal: '*'
            Condition:
              StringNotEquals:
                s3:x-amz-server-side-encryption: 'aws:kms'

          - Sid: DenyUnEncryptedObjectUploads
            Action: s3:PutObject
            Effect: Deny
            Resource: !Sub ${PagesBucket.Arn}/*
            Principal: '*'
            Condition:
              StringNotEquals:
                s3:x-amz-server-side-encryption-aws-kms-key-id: !Ref CMKArn

          - Sid: denyHTTP
            Action: s3:*
            Effect: Deny
            Resource: !Sub ${PagesBucket.Arn}/*
            Principal: '*'
            Condition:
              Bool:
                aws:SecureTransport: 'false' 

  UploadsBucket:
    Type: AWS::S3::Bucket
    Properties:
      BucketName: !Sub ${BucketPrefix}-uploads-${BucketSuffix}
      BucketEncryption:
        ServerSideEncryptionConfiguration:
        - ServerSideEncryptionByDefault:
            SSEAlgorithm: aws:kms
            KMSMasterKeyID: !Ref CMKArn
          BucketKeyEnabled: true            
      PublicAccessBlockConfiguration:
        BlockPublicAcls: true
        BlockPublicPolicy: true
        IgnorePublicAcls: true
        RestrictPublicBuckets: true
      LifecycleConfiguration:
        Rules:
        - Id: IATransition
          Status: Enabled
          Transitions:
          - StorageClass: STANDARD_IA
            TransitionInDays: 30

  UploadsBucketPolicy:
    Type: AWS::S3::BucketPolicy
    Properties:
      Bucket:
        !Ref UploadsBucket
      PolicyDocument:
        Statement:

          - Sid: DenyIncorrectCMK
            Action: s3:PutObject
            Effect: Deny                      
            Resource: !Sub ${UploadsBucket.Arn}/*
            Principal: '*'
            Condition:
              StringNotEquals: 
                s3:x-amz-server-side-encryption-aws-kms-key-id: !Ref CMKArn

          - Sid: DenyIncorrectEncryptionHeader
            Action: s3:PutObject
            Effect: Deny
            Resource: !Sub ${UploadsBucket.Arn}/*
            Principal: '*'
            Condition:
              StringNotEquals:
                s3:x-amz-server-side-encryption: 'aws:kms'

          - Sid: DenyUnEncryptedObjectUploads
            Action: s3:PutObject
            Effect: Deny
            Resource: !Sub ${UploadsBucket.Arn}/*
            Principal: '*'
            Condition:
              StringNotEquals:
                s3:x-amz-server-side-encryption-aws-kms-key-id: !Ref CMKArn

          - Sid: denyHTTP
            Action: s3:*
            Effect: Deny
            Resource: !Sub ${UploadsBucket.Arn}/*
            Principal: '*'
            Condition:
              Bool:
                aws:SecureTransport: 'false' 

  RegistryBucket:
    Type: AWS::S3::Bucket
    Properties:
      BucketName: !Sub ${BucketPrefix}-registry-${BucketSuffix}
      BucketEncryption:
        ServerSideEncryptionConfiguration:
        - ServerSideEncryptionByDefault:
            SSEAlgorithm: aws:kms
            KMSMasterKeyID: !Ref CMKArn
          BucketKeyEnabled: true            
      PublicAccessBlockConfiguration:
        BlockPublicAcls: true
        BlockPublicPolicy: true
        IgnorePublicAcls: true
        RestrictPublicBuckets: true
      LifecycleConfiguration:
        Rules:
        - Id: IATransition
          Status: Enabled
          Transitions:
          - StorageClass: STANDARD_IA
            TransitionInDays: 30

  RegistryBucketPolicy:
    Type: AWS::S3::BucketPolicy
    Properties:
      Bucket:
        !Ref RegistryBucket
      PolicyDocument:
        Statement:

          - Sid: DenyIncorrectCMK
            Action: s3:PutObject
            Effect: Deny                      
            Resource: !Sub ${RegistryBucket.Arn}/*
            Principal: '*'
            Condition:
              StringNotEquals: 
                s3:x-amz-server-side-encryption-aws-kms-key-id: !Ref CMKArn

          - Sid: DenyIncorrectEncryptionHeader
            Action: s3:PutObject
            Effect: Deny
            Resource: !Sub ${RegistryBucket.Arn}/*
            Principal: '*'
            Condition:
              StringNotEquals:
                s3:x-amz-server-side-encryption: 'aws:kms'

          - Sid: DenyUnEncryptedObjectUploads
            Action: s3:PutObject
            Effect: Deny
            Resource: !Sub ${RegistryBucket.Arn}/*
            Principal: '*'
            Condition:
              StringNotEquals:
                s3:x-amz-server-side-encryption-aws-kms-key-id: !Ref CMKArn

          - Sid: denyHTTP
            Action: s3:*
            Effect: Deny
            Resource: !Sub ${RegistryBucket.Arn}/*
            Principal: '*'
            Condition:
              Bool:
                aws:SecureTransport: 'false' 

  BackupBucket:
    Type: AWS::S3::Bucket
    Properties:
      BucketName: !Sub ${BucketPrefix}-backup-${BucketSuffix}
      BucketEncryption:
        ServerSideEncryptionConfiguration:
        - ServerSideEncryptionByDefault:
            SSEAlgorithm: aws:kms
            KMSMasterKeyID: !Ref CMKArn
          BucketKeyEnabled: true            
      PublicAccessBlockConfiguration:
        BlockPublicAcls: true
        BlockPublicPolicy: true
        IgnorePublicAcls: true
        RestrictPublicBuckets: true
      LifecycleConfiguration:
        Rules:
        - Id: IAandDelete
          Status: Enabled
          Transitions:
          - StorageClass: STANDARD_IA
            TransitionInDays: 30
        - Id: DeleteRecords
          Status: Enabled
          ExpirationInDays: 365

  BackupBucketPolicy:
    Type: AWS::S3::BucketPolicy
    Properties:
      Bucket:
        !Ref BackupBucket
      PolicyDocument:
        Statement:

          - Sid: DenyIncorrectCMK
            Action: s3:PutObject
            Effect: Deny                      
            Resource: !Sub ${BackupBucket.Arn}/*
            Principal: '*'
            Condition:
              StringNotEquals: 
                s3:x-amz-server-side-encryption-aws-kms-key-id: !Ref CMKArn

          - Sid: DenyIncorrectEncryptionHeader
            Action: s3:PutObject
            Effect: Deny
            Resource: !Sub ${BackupBucket.Arn}/*
            Principal: '*'
            Condition:
              StringNotEquals:
                s3:x-amz-server-side-encryption: 'aws:kms'

          - Sid: DenyUnEncryptedObjectUploads
            Action: s3:PutObject
            Effect: Deny
            Resource: !Sub ${BackupBucket.Arn}/*
            Principal: '*'
            Condition:
              StringNotEquals:
                s3:x-amz-server-side-encryption-aws-kms-key-id: !Ref CMKArn

          - Sid: denyHTTP
            Action: s3:*
            Effect: Deny
            Resource: !Sub ${BackupBucket.Arn}/*
            Principal: '*'
            Condition:
              Bool:
                aws:SecureTransport: 'false' 

Outputs:
  
  ConfigBucket:
    Description: ConfigBucket
    Value: !Ref ConfigBucket

  ArtifactBucket:
    Description: ArtifactBucket
    Value: !Ref ArtifactBucket

  ExtDiffsBucket:
    Description: ExtDiffsBucket
    Value: !Ref ExtDiffsBucket

  lfsBucket:
    Description: lfsBucket
    Value: !Ref lfsBucket

  PackagesBucket:
    Description: PackagesBucket
    Value: !Ref PackagesBucket

  DepProxyBucket:
    Description: DepProxyBucket
    Value: !Ref DepProxyBucket

  TfStateBucket:
    Description: TfStateBucket
    Value: !Ref TfStateBucket

  PagesBucket:
    Description: PagesBucket
    Value: !Ref PagesBucket

  UploadsBucket:
    Description: UploadsBucket
    Value: !Ref UploadsBucket

  RegistryBucket:
    Description: RegistryBucket
    Value: !Ref RegistryBucket

  BackupBucket:
    Description: BackupBucket
    Value: !Ref BackupBucket
