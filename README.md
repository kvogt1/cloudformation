Added EFS Toggle For Rails
Added Choice between Bring your own or create CMK
Change AMI Selection to the Latest from the Region


# CF-Gitlab-HA-Full

CF for Gitlab Full HA Stack (Non Geo)


[[_TOC_]]


#### Requirements for Basic Stack creation

- Admin IAM User Access Key and Secret.
- S3 Bucket to place all the templates.
- Any TLS/SSL Certificates/Keys.
- A valid **gitlab-secrets.json** file.
- AWS SES User credentials.
    
#### Requirements for Migration

- AWS IAM User with policy to use KMS key for S3 Object Storage Migration.
- Admin access to original GitLab instance.


#### Template Details


Main Template: [_gitlab_ha.cfn.yaml_](gitlab_ha.cfn.yaml)

NOTE: Not in order of recommended deployment

- This template creates the main stack, calling other templates which will create the following templates as nested stacks for each requested resource.

Alerts Template: [_alerts.cfn.yaml_](alerts.cfn.yaml)

- Creates the AWS Log Group and Log Stream resource. It is called from other templates, the values and specifications are customizable based on parameters passed to this template.

FrontEnd Template: [_clb-fe.cfn.yaml_](clb-fe.cfn.yaml)

- Creates Security groups for RDS/Redis, the Classic Load Balancer and the FrontEnd AutoScaling Group. It uses configsets to install/configure OS resources
  and the GitLab software by putting all of that into a Launch Config for AutoScaling Group. It will call the alerts.cfn.yaml for
  Log Group and Log streams.

Databases Template: [_databases.cfn.yaml_](databases.cfn.yaml)

- Template to procure RDS/Redis. It is called by other templates and passed parameters based on naming.

Deploy Gitaly Template: [_deploy_gitaly.cfn.yaml_](deploy_gitaly.cfn.yaml)

- Template to build 3 Gitaly nodes with EBS volumes (LifeCycle Policy attached), by calling the gitaly.cfn.yaml template and sets
  up the Security Group and 3 Route53 entries.
  Uses cfn-init to handle config management. This will be useful for upgrading in the future.

Deploy Praefect Template: [_deploy_praefect.cfn.yaml_](deploy_praefect.cfn.yaml)

- Template to build 3 praefect nodes, 1 RDS, 1 Network Load Balancer for the praefect service by calling the praefect.cfn.yaml template.
  Uses cfn-init to handle config management. This will be useful for upgrading in the future.

EFS Template: [_efs.cfn.yaml_](efs.cfn.yaml)

- Template to create EFS and setup the network for each subnet for connectivity.
  The Pages service for GitLab still requires [NFS](https://docs.gitlab.com/13.7/ee/administration/object_storage.html#gitlab-pages-requires-nfs) to work. 

Gitaly Template: [_gitaly.cfn.yaml_](gitaly.cfn.yaml)

- Template called by deploy_gitaly.cfn.yaml to procure 3 EC2 instances with EBS volumes, calling the alerts.cfn.yaml for Log Groups/Streams.

Praefect Template: [_praefect.cfn.yaml_](praefect.cfn.yaml)

- Template called by deploy_praefect.cfn.yaml to procure 3 EC2 instances, calls alerts.cfn.yaml for Log Groups/Streams, provisions 1 RDS and 1 Network Load Balancer.

GitLab Parameters JSON: [_gitlab_ha_parameters.json_](gitlab_ha_parameters.json)

- Parameters file to set variables based on VPC, Subnets, Instance Types, ARN's for Certificates and EBS LifeCycle Policy, and a few others.

#### Sequence of events

```mermaid
sequenceDiagram 
    participant User
    participant DeployS3 as S3
    participant DeployRole as Roles
    participant DeployEFS as EFS
    participant DeployDBFE as Database Frontend
    participant DeployDBPfct as Database Pfct. 
    participant DeployFE as FrontEnd
    participant DeployGly as Gitaly
    participant DeployPfct as Praefect (Pfct)
    
    Note Right of User : Right arrows are calls to Cloudformation
    Note Right of User : Left arrows are repsonses from loudformation
    Note right of DeployS3 : Stage 1
    User->>DeployS3: All Cond. are false
    activate DeployS3
    DeployS3->>User: S3 buckets deployed
    deactivate DeployS3

    Note right of DeployS3 : Stage 2
    
    par Deploy persistent Storage (Parrallel)
    User->>DeployEFS: DeployEFS: true
    activate DeployEFS
    
    User->>DeployDBFE: DeployDBFE: true
    activate DeployDBFE

    User->>DeployDBPfct: DeployDBPfct: true
    activate DeployDBPfct

    User->>DeployRole: DeployRole: true
    activate DeployRole

    DeployEFS->>User: EFS Deployed 
    deactivate DeployEFS

    DeployDBFE->>User: Fronted DB deployed
    deactivate DeployDBFE

    DeployDBPfct->>User: Pfct DB Deployed
    deactivate DeployDBPfct

    DeployRole->>User: IAM Role Deployed
    deactivate DeployRole
    end

    Note right of DeployS3 : Stage 3
    User->>DeployFE: DeployFE: true
    activate DeployFE
    DeployFE->>User: FrontEnd Deployed
    deactivate DeployFE
    DeployFE->>User: User downloads gitlab-secrets.json from FrontEnd
    User->>DeployS3: Upload gitlab-secrets.json
    
    Note right of DeployS3 : Stage 4
    par Deploy Gitaly and Praefect (Parrallel)
    User->>DeployGly: DeployGly: true
    activate DeployGly
    User->>DeployPfct: DeployPfct: true
    activate DeployPfct
    DeployGly->>User: Gitaly Deployed
    deactivate DeployGly
    DeployPfct->>User: Pfct Deployed
    deactivate DeployPfct
    end
```

#### Initial Set up 
Set **All** booleans to _false_ to deploy only the S3 Buckets, KMS Key, and Iam Profiles which are needed for creating
the base stack.


 >The **config** bucket will need to be populated with a [**gitlab-secrets.json**](#gitlab-secretsjson) file.

```json 
    {
        "ParameterKey": "DeployDBFE",
        "ParameterValue": "false"
    }, 
    {
        "ParameterKey": "DeployDBPfct",
        "ParameterValue": "false"
    }, 
    {
        "ParameterKey": "DeployFE",
        "ParameterValue": "false"
    },
    {
        "ParameterKey": "DeployGly",
        "ParameterValue": "false"
    },
    {
        "ParameterKey": "DeployPfct",
        "ParameterValue": "false"
    },
    {
        "ParameterKey": "DeployEFS",
        "ParameterValue": "false"
    },
    {
        "ParameterKey": "DeployRole",
        "ParameterValue": "false"
    },
    {

```

This will only allow the S3 buckets, RDS Password, CMK, and DBSubnet groups.

Manually upload the [**gitlab-secrets.json**](#gitlab-secrets.json) to the `ConfigBucket` in S3.

Then set the above parameters to true and `update-stack` to build the desired stacks.

### Recommended Sequence

Once the base stack is deployed, turning true other booleans will build out the rest of the infrastructure.

To keep possible roll back time to a minimum, only turn certain ones true and update the stack.

For example, the Databases take the longest to provision/modify so turning those true first and updating the stack will
build them out and prevent long wait times if something fails and a rollback is forced.

#### Build data bases (i.e. `DB`)
```json
    {
        "ParameterKey": "DeployDBFE",
        "ParameterValue": "true"
    }, 
    {
        "ParameterKey": "DeployDBPfct",
        "ParameterValue": "true"
    }, 
    {
        "ParameterKey": "DeployFE",
        "ParameterValue": "false"
    },
    {
        "ParameterKey": "DeployGly",
        "ParameterValue": "false"
    },
    {
        "ParameterKey": "DeployPfct",
        "ParameterValue": "false"
    },
    {
        "ParameterKey": "DeployEFS",
        "ParameterValue": "false"
    },
    {
        "ParameterKey": "DeployRole",
        "ParameterValue": "true"
    },
```

Once the databases are provisioned, turn on the FE boolean (this requires EFS and DeployRole be set to true due to dependant resources).

#### Build FrontEnd (i.e. `FE`)

```json
    {
        "ParameterKey": "DeployDBFE",
        "ParameterValue": "true"
    }, 
    {
        "ParameterKey": "DeployDBPfct",
        "ParameterValue": "true"
    }, 
    {
        "ParameterKey": "DeployFE",
        "ParameterValue": "true"
    },
    {
        "ParameterKey": "DeployGly",
        "ParameterValue": "false"
    },
    {
        "ParameterKey": "DeployPfct",
        "ParameterValue": "false"
    },
    {
        "ParameterKey": "DeployEFS",
        "ParameterValue": "true"
    },
    {
        "ParameterKey": "DeployRole",
        "ParameterValue": "true"
    },
```

For Gitaly, set the DeployGly boolean to _true_. You can also set the praefect to _true_, however separating them in case of forced rollback
is probably the best option.

#### Build Gitaly and Praefect (i.e. `Gly` and `Pfct`)

```json
    {
        "ParameterKey": "DeployDBFE",
        "ParameterValue": "true"
    }, 
    {
        "ParameterKey": "DeployDBPfct",
        "ParameterValue": "true"
    }, 
    {
        "ParameterKey": "DeployFE",
        "ParameterValue": "true"
    },
    {
        "ParameterKey": "DeployGly",
        "ParameterValue": "true"
    },
    {
        "ParameterKey": "DeployPfct",
        "ParameterValue": "true"
    },
    {
        "ParameterKey": "DeployEFS",
        "ParameterValue": "true"
    },
    {
        "ParameterKey": "DeployRole",
        "ParameterValue": "true"
    },
```


#### gitlab-secrets.json

##### Migrate Infra

If the intention is to create infrastructure to migrate into from another GitLab instance, re-use the 
**gitlab-secrets.json** file by placing it into the **config** bucket after initial stack creation when
all booleans are false in the parameters.json file

##### New Infra

If the intention is to create a whole new instance infrastructure, you will need to create the **gitlab-secrets.json** by
setting the following values to **true** and update the stack:

- "DeployDBFE"
- "DeployFE"
- "DeployRole"
- "DeployEFS"

Once the FrontEnd EC2 instance is in the ready state, use SSM to connect to the instance.

Get the contents of the _/etc/gitlab/gitlab-secrets.json_ and put into a file locally named **gitlab-secrets.json**. 

Then upload this file to the **config** bucket.

### Migration

These steps are listed only for a migration to this new infrastructure from another GitLab installation.

#### Deploy Initial Base Stack

Create the S3 bucket where all templates will be read from.

Edit the [parameters](gitlab_ha_parameters.json) file and set all booleans to **false**.

Create the stack. This only creates the S3 buckets necessary for object storage, the IAM Roles/Policies, and the KMS key.

An IAM user may need to be created that has access to the S3 policies, and KMS policies.

Test the connectivity works: (requires aws cli to be installed, and possibly credentials stored/read in)

```bash
root@old-instance:/>aws s3 ls
```

A list of S3 buckets should show.

Use the following GitLab Documentation to configure the _gitlab.rb_ on the original instance: [Object Storage](https://docs.gitlab.com/ee/administration/object_storage.html#consolidated-object-storage-configuration).

For :
Uploads
Artifacts
LFS
Packages

Then reconfigure the gitlab instance:

```bash
root@old-instance:/> gitlab-ctl reconfigure
```

Fix any errors.

Follow the migration steps for the consolidated object storage buckets noted [here](https://docs.gitlab.com/charts/installation/migration/#migration-steps)
> NOTE: Registry connectivity will be different, follow these [steps](https://docs.gitlab.com/13.6/ee/administration/packages/container_registry.html#use-object-storage) to configure.

Migrate existing files (uploads, artifacts, lfs objects) from package based installation to object storage.

Migrate artifacts to object storage:
```bash
root@old-instance:/>sudo gitlab-rake gitlab:artifacts:migrate
```

Migrate existing LFS objects to object storage:

```bash
root@old-instance:/>sudo gitlab-rake gitlab:lfs:migrate
```

Migrate existing Packages to object storage:

```bash
root@old-instance:/>sudo gitlab-rake gitlab:packages:migrate
```

Migrate existing uploads to object storage
```bash
root@old-instance:/>sudo gitlab-rake gitlab:uploads:migrate:all
```

[See documentation.](https://docs.gitlab.com/ee/administration/raketasks/uploads/migrate.html#migrate-to-object-storage)

Migrate existing Registry Images to object storage: [Migrate without downtime](https://docs.gitlab.com/13.6/ee/administration/packages/container_registry.html#migrate-to-object-storage-without-downtime).


#### Backup/Restore

On original GitLab instance, perform backup skipping the object storage items: [skip components](https://docs.gitlab.com/charts/architecture/backup-restore.html#command-line-arguments).

Copy backup file to **backup** S3 bucket.

Copy the [gitlab-secrets.json](#gitlab-secrets) file from original gitlab instance to **config** bucket.

Update the cloudformation stack by following the [Recommended Sequence](#recommended-sequence) and build out the rest of the necessary resources.

Wait for stack to complete. Validate the URL.

On new AWS FrontEnd EC2 instance (connect via SSM), restore with the `-t` [parameter](https://docs.gitlab.com/charts/architecture/backup-restore.html#restore).

Run the [Gitlab backup Rake Task](https://gitlab.com/gitlab-org/gitlab-foss/tree/master/lib/tasks/gitlab/backup.rake).


### Upgrades

#### Using CFN-INIT

Start by updating the clb-fe.cfn.yaml, gitaly.cfn.yaml, and praefect.cfn.yaml templates config set sections to update the package version of GitLab

> gitlab-ee-13.6.2: []

Then upload the latest templates to the S3 bucket used to deploy the CloudFormation.

Go to AWS Console > CloudFormation > Gitlab Stack.

Click "update stack" button and point to the S3 bucket where templates are stored (or use the launch script, details to be aded).

Watch the CloudFormation Stack progress to "Update Complete".

Next go to AWS Console > EC2 > Running Instances

Per the type of EC2 (FrontEnd, Gitaly/Praefect) follow the next steps

All EC2 deployment templates use _cfn-init_ config sets to install/configure software and OS changes necessary for the GitLab Suite.

##### FrontEnd

FrontEnd Instances use AutoScaling, along with _cfn-init_ and present 2 paths for upgrades/changes.

1. Run the script created in _/root/cfn-init.sh_ 
   > This has all the config sets copied into a script, log in via SSM, sudo to root and run the script:

```bash
$>sudo -i
root@instance:/root>source ./cfn-init.sh
```

Tail the /var/log/cfn-init.log to watch for completion.

2. Delete the instance during a maintenance window and allow the AutoScaling Launch Config provision a new instance with updated config.

##### Gitaly/Praefect

Gitaly and Praefect deployment templates use _cfn-init_ config sets to install/configure software and OS changes necessary for the GitLab Suite.

There is only one path to upgrade the GitLab suite on the Gitaly and Praefect instances.
1. Run the script created in _/root/cfn-init.sh_ 
   > This has all the config sets copied into a script, log in via SSM, sudo to root and run the script:

```bash
$>sudo -i
root@instance:/root>source ./cfn-init.sh
```

Tail the /var/log/cfn-init.log to watch for completion.